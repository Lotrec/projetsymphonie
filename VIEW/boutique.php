<?php 
$_SESSION = 'admin';
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Panier</title>
    <link rel="stylesheet" href="cnc.css">
</head>
<body>
<?php 
$page = 'Boutique';
include '../VIEW/navbar.php';
include '../MODEL/readAll.php'
?>
<div id="liste">
    <?php


        foreach($repPdt as $data){
    ?>

        <div class="produit">
            <img src="<?= $data['image'] ?>" alt="<?= $data['nom'] ?>">
            <div>
                <h2><?= $data['nom'] ?></h2>
                <h4><?= $data['description'] ?></h4>
                <p><span><?= $data['prix'] ?></span>€</p>
                <p><audio controls preload="auto" src="<?= $data['son'] ?>"></audio></p>
            </div>
            <div>
                <form action="../CONTROL/ajouter.php" method="post">
                    <input type="hidden" name="id_prod" value="<?= $data['id'] ?>">
                    <input type="hidden" name="id" value="<?= $data['id'] ?>">
                    <input name="qt" type="hidden" value="1">
                    <input name="dispo" type="hidden" value="0">
                    <?php if ($data['dispo'] == 1) {?>
                    <input type="submit" value="Ajouter au panier">
                <?php }
                else {?>
                Produit indisponible
                <?php }?>
                <?php
if($_SESSION == 'admin'){?>
    <a href="../VIEW/modifierPdt.php">Modifier</a>
    <?php }
    else {?>
<?php }
?>  
                    </form>
            </div>
    </div>

    <?php } ?>
        <a href="validation.php">Valider mon panier</a>
        <div>
                            <a href=../CONTROL/updatePdtStock.php>updateProduits</a>
                            <a href=../VIEW/adminPdt.php>Add Produit</a>
                </div>
    </div>
</body>
</html>